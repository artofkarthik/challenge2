package com.challenge.two

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel: ViewModel() {

    var inputText = MutableLiveData<String>()

    fun onClickExecute() {
        Log.d("MainActivityViewModel", inputText.value.toString())
    }

}